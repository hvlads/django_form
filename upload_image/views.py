from django.shortcuts import render
from .forms import UploadFileForm

def upload_image(request):
    form = UploadFileForm()
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            pass

    return render(request, 'index.html', {'form': form})
